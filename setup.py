from setuptools import setup, find_packages

setup(
    name='b2btest',
    version='0.1',
    author="bio2Byte group",
    author_email='test@vub.be',
    packages=find_packages(),
)